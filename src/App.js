import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import Login from './pages/Login';
import ItemCard from './components/ItemCard';


function App() {
	return(
		<Router>
			<NavBar/>
			<Container>
				<Routes>
					<Route exact path ="/login" element={<Login/>} />
          <Route exact path="/items" element={<ItemCard/>} />
				</Routes>
			</Container>
		</Router>
	);
  
}

export default App;

