import {Card, Button, Container} from 'react-bootstrap';


export default function xCard() {

return (


    <Container className='card1'>
    <Card style={{ width: '30rem' }} className="mx-5 my-5 shadow-5 hover-shadow" >
             <Card.Img variant="top" src="./images/Desai.jpg" />
             <Card.Body>
             <Card.Title>New Arrivals</Card.Title>
         <Card.Text className='mb-2 text-muted'>
             This item is for sale! 
         </Card.Text>
         <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
         <Card.Text>3000.00</Card.Text>
        <Button variant="primary" className='hover-shadow'>Buy Now!</Button>
        </Card.Body>
    </Card>
    </Container>
    );
    
}