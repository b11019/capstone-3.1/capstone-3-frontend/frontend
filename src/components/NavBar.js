import {Navbar, Container, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { FiShoppingCart } from "react-icons/fi";
import{Row, Col, Card, Dropdown, Badge} from 'react-bootstrap';


export default function NavBar() {

	return(
	<>
			<Navbar bg="dark" expand="lg" className=" navMain pt-4 pb-4">

			    <Container>
			        <Navbar.Brand className="text-light me-5">Insert Name Here</Navbar.Brand>
			        <form class="container-fluid">
			            <div class="input-group">
			              <input type="text" class="form-control me-2" aria-describedby="basic-addon1"/>
			              <button class="btn btn-outline-light" type="submit">Search</button>
			            </div>
			    	</form>
		    	    <Nav.Link as={Link} to="/Login" className="text-light">Login</Nav.Link>
		    	    <Nav.Link className="text-light">SignUp</Nav.Link>		 
			    	<Nav>
			    		<Dropdown alignRight>
			    			<Dropdown.Toggle variant="dark">
			    				<FiShoppingCart size={30}/>
			    				<Badge bg="dark">{10}</Badge>
			    			</Dropdown.Toggle>

			    			<Dropdown.Menu style ={{minWidth:370}}>
			    				<span>Cart is empty</span>
			    			</Dropdown.Menu>
			    		</Dropdown>
			    	</Nav>
			    </Container>
			</Navbar>

			<Navbar bg="light" expand="lg">
	            <Container> 

	            	<div class="dropdown">
	            	  <Nav class=" btn dropdown-toggle me-4" href="#"  id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
	            	    FEATURED
	            	  </Nav>
	            	</div>

	            	<div class="dropdown">
	            	  <Nav class="btn  dropdown-toggle me-4" href="#"  id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
	            	    ITEM CATEGORY 1
	            	  </Nav>
	            	</div>

	            	<div class="dropdown">
	            	  <Nav class="btn  dropdown-toggle me-4" href="#"  id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
	            	    ITEM CATEGORY 2
	            	  </Nav>
	            	</div>

            	  	<div class="dropdown">
	            	  <Nav class="btn  dropdown-toggle me-4" href="#"  id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
	            	    ITEM CATEGORY 3
	            	  </Nav>
	            	</div>

	            	<div class="dropdown">
	            	  <Nav class="btn  dropdown-toggle me-4" href="#"  id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
	            	    ITEM CATEGORY 4
	            	  </Nav>
	            	</div>


	             </Container>
	    
	        </Navbar>
	</>
	)
}
