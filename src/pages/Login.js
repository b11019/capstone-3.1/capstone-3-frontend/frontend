import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card} from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';



export default function Login(props) {

    return (

        <Container className="w-50">
            
            <h4 className= "mt-5" style={{
                display: "flex",
                justifyContent: "center"
                 }}>
            Login</h4>

            <Card classname="loginCard" class="shadow p-3 mb-5 bg-white rounded">
                <Card.Body>
                    <Card.Text className="mb-4" style={{
                        display: "flex",
                        justifyContent: "center"
                         }}>
                        Enter your username and password:   
                    </Card.Text>
                    <Form >
                        <Form.Group className="mb-3">
                            
                            <Form.Control 
                                type="text" 
                                placeholder="Username"

                            />
                        </Form.Group>

                        <Form.Group controlId="Password">
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                            />
                        </Form.Group>   
                        <div style={{
                            display: "flex",
                            justifyContent: "center"
                             }}>
                            <Button variant="dark" type="submit" id="submitBtn" className="mt-3 mb-3 ">
                                Submit
                            </Button>
                        </div>

                        <div style={{
                            display: "flex",
                            justifyContent: "center"
                             }}>
                            <text className="pt-2 ">No account yet?</text>
                            <Button className="text-primary" variant="muted" as={Link} to="/">Register here!</Button>

                        </div>

                    </Form>

                </Card.Body>


            </Card>
        
        </Container>

    )

}
